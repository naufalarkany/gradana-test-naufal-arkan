## Start Node Server

- Run `npm install` to install required packages
- Open terminal run `nodemon`
- Open other terminal run `mongod`

Open API URL on [http://localhost:4000/api](http://localhost:4000/api)
