const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const userSchema = require("../models/User");
const { validationResult } = require("express-validator");

const registerUser = (req, res, next) => {
  const errors = validationResult(req);
  console.log(req.body);

  if (!errors.isEmpty()) {
    return res.status(422).jsonp(errors.array());
  } else {
    bcrypt.hash(req.body.password, 10).then((hash) => {
      const user = new userSchema({
        name: req.body.name,
        email: req.body.email,
        mobile: req.body.mobile,
        balance: 0,
        password: hash,
        history: [
          {
            topup: 0,
            date: Date.now(),
          },
        ],
      });
      user
        .save()
        .then((response) => {
          res.status(201).json({
            message: "User successfully created!",
            result: response,
          });
        })
        .catch((error) => {
          res.status(500).json({
            error: error,
          });
        });
    });
  }
};

const login = (req, res, next) => {
  let getUser;
  userSchema
    .findOne({
      email: req.body.email,
    })
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          message: "Authentication failed email",
        });
      }
      getUser = user;
      return bcrypt.compare(req.body.password, user.password);
    })
    .then((response) => {
      if (!response) {
        return res.status(401).json({
          message: "Authentication failed password",
        });
      }
      let jwtToken = jwt.sign(
        {
          email: getUser.email,
          userId: getUser._id,
        },
        "longer-secret-is-better",
        {
          expiresIn: "1h",
        }
      );
      res.status(200).json({
        token: jwtToken,
        expiresIn: 3600,
        _id: getUser._id,
      });
    })
    .catch((err) => {
      return res.status(401).json({
        message: "Authentication failed",
      });
    });
};

const getUser = (req, res, next) => {
  userSchema.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        data: data,
      });
    }
  });
};

const topup = (req, res, next) => {
  userSchema.findByIdAndUpdate(
    req.params.id,
    {
      balance: req.body.balanceBefore + req.body.topupBalance,
      $push: {
        history: {
          topup: req.body.topupBalance,
          date: Date.now(),
        },
      },
    },
    (error, data) => {
      if (error) {
        return next(error);
      } else {
        res.json("Top Up successfully updated!");
      }
    }
  );
};

module.exports = {
  registerUser,
  login,
  getUser,
  topup,
};
