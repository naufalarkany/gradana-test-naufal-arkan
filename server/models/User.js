const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");

let userSchema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
    },
    mobile: {
      type: String,
      unique: true,
    },
    balance: {
      type: Number,
    },
    password: {
      type: String,
    },
    history: [
      {
        topup: { type: Number },
        date: { type: Date },
      },
    ],
  },
  {
    collection: "users",
  }
);

userSchema.plugin(uniqueValidator, {
  message: "Email or Mobile Phone already in use.",
});
module.exports = mongoose.model("User", userSchema);
