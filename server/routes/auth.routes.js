const express = require("express");
const router = express.Router();
const userSchema = require("../models/User");
const authorize = require("../middlewares/auth");
const userController = require("../controller/user.controller.js");
const { check } = require("express-validator");

// Sign-up
router.post(
  "/register-user",
  [
    check("name")
      .not()
      .isEmpty()
      .isLength({ min: 3 })
      .withMessage("Name must be atleast 3 characters long"),
    check("email", "Email is required").not().isEmpty(),
    check("mobile", "Mobile number is required").not().isEmpty(),
    check("password", "Password should be at least 5 characters long")
      .not()
      .isEmpty()
      .isLength({ min: 5 }),
  ],
  userController.registerUser
);

// Sign-in
router.post("/signin", userController.login);

// Get Users
router.route("/").get((req, res, next) => {
  userSchema.find((error, response) => {
    if (error) {
      return next(error);
    } else {
      return res.status(200).json(response);
    }
  });
});

// Get Single User
router.route("/user-balance/:id").get(authorize, userController.getUser);

// Update User
router.route("/topup/:id").put(authorize, userController.topup);

module.exports = router;
