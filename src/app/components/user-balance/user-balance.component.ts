import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-balance',
  templateUrl: './user-balance.component.html',
  styleUrls: ['./user-balance.component.css'],
})
export class UserBalanceComponent implements OnInit {
  currentUser: any = {};
  constructor(
    public authService: AuthService,
    private actRoute: ActivatedRoute,
    public router: Router
  ) {
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.authService.getUserProfile(id).subscribe((res) => {
      this.currentUser = res.data;
      console.log(this.currentUser);
    });
  }
  history() {
    this.router.navigate([`user-history/${this.currentUser._id}`]);
  }
  ngOnInit() {}
}
