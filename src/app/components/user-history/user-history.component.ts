import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css'],
})
export class UserHistoryComponent implements OnInit {
  currentUser: any = {};
  topupHistory: any = [];
  constructor(
    public authService: AuthService,
    private actRoute: ActivatedRoute,
    public router: Router
  ) {
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.authService.getUserProfile(id).subscribe((res) => {
      this.currentUser = res.data;
      this.topupHistory = res.data.history;
      console.log(this.topupHistory);
    });
  }
  balance() {
    this.router.navigate([`user-balance/${this.currentUser._id}`]);
  }
  ngOnInit() {}
}
