export class User {
  _id!: String;
  name!: String;
  mobile!: String;
  balance!: Number;
  email!: String;
  password!: String;
}
